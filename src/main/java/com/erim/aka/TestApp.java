package com.erim.aka;

import com.erim.aka.model.MarsRover;
import com.erim.aka.model.enums.FACE;
import com.erim.aka.model.exceptions.RoverException;

public class TestApp
{

    public static void main(String[] args)
    {
        try
        {
            MarsRover marsRovers = new MarsRover(5, 5);
            marsRovers.addRover(1, 2, FACE.NORTH);
            marsRovers.addCommand("LMLMLMLMM");
            
            //exception case
//            marsRovers.addRover(7, 3, FACE.EAST);
//            marsRovers.addCommand("MMRMMRMRRM");
            
            marsRovers.addRover(3, 3, FACE.EAST);
            marsRovers.addCommand("MMRMMRMRRM");
            
            marsRovers.getFinalPositions();
        }
        catch (RoverException exception) {
           System.out.println(exception.getMessage());
        }

    }

}
