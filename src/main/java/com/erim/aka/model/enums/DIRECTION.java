package com.erim.aka.model.enums;

import lombok.Getter;

public enum DIRECTION
{

    LEFT("L", -1), RIGHT("R", 1);

    @Getter
    private String kod;

    @Getter
    private int deger;

    private DIRECTION(String kod, int deger)
    {
        this.kod = kod;
        this.deger = deger;
    }

}
