package com.erim.aka.model.enums;

import lombok.Getter;

public enum COMMAND
{
    MOVE("M");

    @Getter
    private String value;

    private COMMAND(String value)
    {
        this.value = value;
    }

}
