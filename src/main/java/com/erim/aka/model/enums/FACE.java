package com.erim.aka.model.enums;

import lombok.Getter;

public enum FACE
{
    NORTH("N", 1, 1), EAST("E", 2, 1), SOUTH("S", 3, -1), WEST("W", 4, -1);

    @Getter
    private String kod;

    @Getter
    private int deger;

    @Getter
    private int moveValue;

    private FACE(String kod, int deger, int moveValue)
    {
        this.kod = kod;
        this.deger = deger;
        this.moveValue = moveValue;
    }

}
