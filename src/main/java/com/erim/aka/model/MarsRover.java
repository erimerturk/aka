package com.erim.aka.model;

import java.util.List;

import lombok.Data;

import org.apache.commons.lang.StringUtils;

import com.erim.aka.model.enums.FACE;
import com.erim.aka.model.exceptions.RoverException;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

@Data
public class MarsRover
{

    private Bound bound;
    private Rover currentRover;
    private List<Rover> roverList = Lists.newArrayList();

    public MarsRover(int maxX, int maxY)
    {
        this.bound = new Bound(maxX, maxY);
    }

    public void addRover(int x, int y, FACE face)
    {
        try
        {
            bound.checkCoordinateIsInRange(x, y);
            Rover newRover = null;
            if (currentRover != null)
            {
                Preconditions.checkNotNull(currentRover.getCommand(), "Before new rover you have to finish current one. Send a command.");
                newRover = new Rover(x, y, face);
            }
            else
            {
                newRover = new Rover(x, y, face);
            }
            newRover.setBound(bound);
            currentRover = newRover;
        }
        catch (Exception exception)
        {
            throw new RoverException(" Exception occured while trying to add Rover : " + exception.getMessage());
        }
    }

    public void addCommand(String command) throws RoverException
    {
        try
        {
            Preconditions.checkNotNull(currentRover, "You haven't created any rover.");
            Preconditions.checkArgument(currentRover.getCommand() == null, "You have already define a command for current rover.");
            Preconditions.checkArgument(!StringUtils.isBlank(command), "Command is not valid");
            currentRover.setCommand(command);
            currentRover.move();
            roverList.add(currentRover);
        }
        catch (Exception exception)
        {
            throw new RoverException("Exception occured while trying to add Rover : " + exception.getMessage());
        }
    }

    public void getFinalPositions()
    {
        for (Rover rover : getRoverList())
        {
            System.out.println(rover.getPoint().toString());
        }
    }
}
