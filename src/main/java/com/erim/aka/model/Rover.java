package com.erim.aka.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.erim.aka.model.enums.COMMAND;
import com.erim.aka.model.enums.FACE;
import com.erim.aka.model.exceptions.RoverException;
import com.erim.aka.util.EnumsUtil;
import com.google.common.base.Preconditions;

@EqualsAndHashCode(exclude = { "command", "bound" })
public class Rover
{
    @Getter
    @Setter
    Point point;

    @Getter
    @Setter
    private String command;

    @Getter
    @Setter
    private Bound bound;

    public Rover(int x, int y, FACE face)
    {
        this.point = new Point(x, y, face);
    }

    public void move() throws RoverException
    {
        for (char command : getCommand().toCharArray())
        {
            if (isDirection(command))
            {
                this.point.changeDirection(command);
            }
            else if (isMove(command))
            {
                if (this.point.willMoveOnXCoordinate())
                {
                    this.point.moveOnXCoordinate();
                }
                else if (this.point.willMoveOnYCoordinate())
                {
                    this.point.moveOnYCoordinate();
                }
                Preconditions.checkArgument(!isInVaildPosition(), "Rover will be out of range");
            }
            else
            {
                throw new RoverException("Command is not valid : " + command);
            }
        }
    }

    private boolean isDirection(char value)
    {
        return EnumsUtil.getDirectionByValue(value) != null;
    }

    private boolean isInVaildPosition()
    {
        return this.point.getX() > this.bound.getX() || this.point.getY() > this.bound.getY();
    }

    private boolean isMove(char value)
    {
        return COMMAND.MOVE.getValue().equals(String.valueOf(value));
    }

}
