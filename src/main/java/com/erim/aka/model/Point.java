package com.erim.aka.model;

import lombok.Data;

import com.erim.aka.model.enums.FACE;
import com.erim.aka.util.EnumsUtil;
import com.google.common.base.Preconditions;


@Data
public class Point
{
    private int x;
    private int y;
    private FACE face;
    
    public Point(int x, int y, FACE face)
    {
        this.x = x;
        this.y = y;
        this.face = face;
    }
    
    public void moveOnXCoordinate()
    {
        int newX = getX() + face.getMoveValue();
        Preconditions.checkArgument(newX >= 0, "Rover will be out of range");
        this.x = newX;
    }
    
    public void moveOnYCoordinate()
    {
        int newY = getY() + face.getMoveValue();
        Preconditions.checkArgument(newY >= 0, "Rover will be out of range");
        this.y = newY;
    }
    
    public void changeDirection(char direction)
    {
        int newFaceValue = this.getFace().getDeger() + EnumsUtil.getDirectionByValue(direction).getDeger();
        newFaceValue = newFaceValue > FACE.WEST.getDeger() ? FACE.NORTH.getDeger() : newFaceValue;
        newFaceValue = newFaceValue < FACE.NORTH.getDeger() ? FACE.WEST.getDeger() : newFaceValue;
        this.face = EnumsUtil.getFaceByValue(newFaceValue);
    }
    
    public boolean willMoveOnXCoordinate()
    {
        return getFace().equals(FACE.WEST) || getFace().equals(FACE.EAST);
    }
    
    public boolean willMoveOnYCoordinate()
    {
        return getFace().equals(FACE.NORTH) || getFace().equals(FACE.SOUTH);
    }
    
    @Override
    public String toString()
    {
        return new StringBuffer()
                        .append("x : ")
                        .append(getX())
                        .append(" y : ")
                        .append(getY())
                        .append(" face : ")
                        .append(getFace().getKod())
                        .toString();
    }

}
