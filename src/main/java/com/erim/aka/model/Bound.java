package com.erim.aka.model;

import com.google.common.base.Preconditions;

import lombok.Data;

@Data
public class Bound
{
    private int x;
    private int y;
    
    public Bound(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public void checkCoordinateIsInRange(int x, int y)
    {
        Preconditions.checkArgument(x >= 0, "x coordinate cant be smaller than 0");
        Preconditions.checkArgument(y >= 0, "y coordinate cant be smaller than 0");
        Preconditions.checkArgument(this.x >= x, "x coordinate cant be bigger than " + this.x);
        Preconditions.checkArgument(this.y >= y, "y coordinate cant be bigger than " + this.y);
    }
}