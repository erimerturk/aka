package com.erim.aka.model.exceptions;

@SuppressWarnings("serial")
public class RoverException extends RuntimeException
{
    public RoverException(String message) {
        super(message);
    }
}
