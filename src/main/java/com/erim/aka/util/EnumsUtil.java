package com.erim.aka.util;

import com.erim.aka.model.enums.DIRECTION;
import com.erim.aka.model.enums.FACE;

public class EnumsUtil
{
    
    public static DIRECTION getDirectionByValue(char value)
    {
        for (DIRECTION direction : DIRECTION.values())
        {
            if (direction.getKod().equals(String.valueOf(value)))
            {
                return direction;
            }
        }

        return null;
    }
    
    public static FACE getFaceByValue(int value)
    {
        for (FACE face : FACE.values())
        {
            if (face.getDeger() == value)
            {
                return face;
            }
        }
        return null;
    }

}
