package com.erim.aka;

import static org.junit.Assert.assertNotNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;

import com.erim.aka.model.Point;
import com.erim.aka.model.enums.FACE;

public class PointTest extends UnitilsJUnit4
{
    Point point;

    @Before
    public void init()
    {
        point = new Point(3, 3, FACE.EAST);
    }

    @Test
    public void testCreatePoint()
    {
        assertNotNull(point);
    }
    
    @Test
    public void testChangeDirection()
    {
        point.changeDirection('R');
        assertReflectionEquals("Change direction is not worked", point, new Point(3, 3, FACE.SOUTH));
    }
    
    @Test
    public void testmMveOnXCoordinate()
    {
        point.moveOnXCoordinate();
        assertReflectionEquals("Change direction is not worked", point, new Point(4, 3, FACE.EAST));
    }
    
    @Test
    public void testmMveOnYCoordinate()
    {
        point.moveOnYCoordinate();
        assertReflectionEquals("Change direction is not worked", point, new Point(3, 4, FACE.EAST));
    }

}
