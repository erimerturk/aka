package com.erim.aka;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;

import com.erim.aka.model.MarsRover;
import com.erim.aka.model.enums.FACE;
import com.erim.aka.model.exceptions.RoverException;

public class MarsRoverTest extends UnitilsJUnit4
{

    @Test
    public void testCreateMarsLand()
    {
        MarsRover rover = new MarsRover(5, 5);
        assertReflectionEquals("x coordinates are not equal", 5, rover.getBound().getX());
        assertReflectionEquals("y coordinates are not equal", 5, rover.getBound().getY());
    }

    @Test(expected = RoverException.class)
    public void testAddInvaildRoversToMarsLand()
    {
        MarsRover land = new MarsRover(7, 7);
        land.addRover(-1, 2, FACE.EAST);
        land.addRover(1, 8, FACE.EAST);
    }

    @Test(expected = RoverException.class)
    public void testAddVaildRoversToMarsLand()
    {
        MarsRover land = new MarsRover(7, 7);
        land.addRover(1, 2, FACE.EAST);
        land.addRover(7, 7, FACE.EAST);
    }

    @Test
    public void testSendCommand()
    {
        MarsRover land = new MarsRover(7, 7);
        land.addRover(1, 2, FACE.NORTH);
        land.addCommand("LMLMLMLMM");
    }
    
    @Test(expected = RoverException.class)
    public void testSendEmptyCommand()
    {
        MarsRover land = new MarsRover(7, 7);
        land.addRover(1, 2, FACE.NORTH);
        land.addCommand("");
    }
    
    @Test(expected = RoverException.class)
    public void testSendCommandTwice()
    {
        MarsRover land = new MarsRover(7, 7);
        land.addRover(1, 2, FACE.NORTH);
        land.addCommand("LMLMLMLMM");
        land.addCommand("RMRM");
    }

    @Test(expected = RoverException.class)
    public void testOutOfRange()
    {
        MarsRover land = new MarsRover(5, 5);
        land.addRover(1, 2, FACE.NORTH);
        land.addCommand("MMMMMMMMM");
    }

}
