package com.erim.aka;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;

import com.erim.aka.model.Bound;
import com.erim.aka.model.Rover;
import com.erim.aka.model.enums.FACE;
import com.erim.aka.model.exceptions.RoverException;

public class RoverTest extends UnitilsJUnit4
{

    @Test
    public void testCreateRover()
    {
        Rover rover = init();
        assertReflectionEquals("x coordinates are not equal", 1, rover.getPoint().getX());
        assertReflectionEquals("x coordinates are not equal", 2, rover.getPoint().getY());
        assertReflectionEquals("faces are not equal", FACE.NORTH, rover.getPoint().getFace());
    }

    private Rover init()
    {
        Rover rover = new Rover(1, 2, FACE.NORTH);
        rover.setBound(new Bound(5, 5));
        return rover;
    }
    
    @Test
    public void testTurnLeft()
    {
        Rover rover = init();
        rover.setCommand("L");
        rover.move();
        Rover expected = new Rover(1, 2, FACE.WEST);
        assertEquals(expected, rover);
    }

    @Test
    public void testTurnLeftTwice()
    {
        Rover rover = init();
        rover.setCommand("LL");
        rover.move();
        Rover expected = new Rover(1, 2, FACE.SOUTH);
        assertEquals(expected, rover);
    }

    @Test
    public void testTurnLeftTwiceAndRight()
    {
        Rover rover = init();
        rover.setCommand("LLR");
        rover.move();
        Rover expected = new Rover(1, 2, FACE.WEST);
        assertEquals(expected, rover);
    }

    @Test
    public void testMoveOnXCoordinate()
    {
        Rover rover = new Rover(1, 2, FACE.EAST);
        rover.setBound(new Bound(5, 5));
        rover.setCommand("M");
        rover.move();
        Rover expected = new Rover(2, 2, FACE.EAST);
        assertEquals(expected, rover);
    }

    @Test
    public void testMoveOnYCoordinate()
    {
        Rover rover = new Rover(1, 2, FACE.EAST);
        rover.setBound(new Bound(5, 5));
        rover.setCommand("RM");
        rover.move();
        Rover expected = new Rover(1, 1, FACE.SOUTH);
        assertEquals(expected, rover);
    }

    @Test
    public void testComplexMove()
    {
        Rover rover = init();
        rover.setCommand("LMLMLMLMM");
        rover.move();
        Rover expected = new Rover(1, 3, FACE.NORTH);
        assertEquals(expected, rover);
    }

    @Test
    public void testComplexMove2()
    {
        Rover rover = new Rover(3, 3, FACE.EAST);
        rover.setBound(new Bound(5, 5));
        rover.setCommand("MMRMMRMRRM");
        rover.move();
        Rover expected = new Rover(5, 1, FACE.EAST);
        assertEquals(expected, rover);
    }

    @Test(expected = RoverException.class)
    public void testInvalidCommand()
    {
        Rover rover = new Rover(3, 3, FACE.EAST);
        rover.setBound(new Bound(5, 5));
        rover.setCommand("RFX");
        rover.move();
    }
}
