package com.erim.aka;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;

import com.erim.aka.model.Bound;

public class BoundTest extends UnitilsJUnit4
{
    Bound bound;
    
    @Before
    public void init()
    {
        bound = new Bound(5, 5);
    }
    
    @Test
    public void testCreateBound()
    {
        assertNotNull(bound);
    }

    @Test
    public void testValidCoordinate()
    {
        bound.checkCoordinateIsInRange(3, 3);
    }
    
    @Test(expected =IllegalArgumentException.class)
    public void testYBiggerThanBound()
    {
        bound.checkCoordinateIsInRange(1, 7);
    }
    
    @Test(expected =IllegalArgumentException.class)
    public void testXBiggerThanBound()
    {
        bound.checkCoordinateIsInRange(7, 1);
    }
    
    @Test(expected =IllegalArgumentException.class)
    public void testNegativeXCoordinate()
    {
        bound.checkCoordinateIsInRange(-1, 1);
    }
    
    @Test(expected =IllegalArgumentException.class)
    public void testNegativeYCoordinate()
    {
        bound.checkCoordinateIsInRange(1, -1);
    }
   
}
